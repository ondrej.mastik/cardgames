Cardgames Project
=================

This project was created just because I was bored and wanted to do something.

It's goal is to create my own platform where I can play various games I like.

Requirements
------------

PHP 7.3 or higher, apache server, MariaDB, Composer, Yarn

Installation
------------

Composer install
cd www
yarn install
cd ..
mkdir temp temp/cache

Make directories `temp/` and `log/` writable.

How to run
------------

To be done